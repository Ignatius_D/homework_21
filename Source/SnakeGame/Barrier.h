// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Barrier.generated.h"

class ABarrierElement;

UCLASS()
class SNAKEGAME_API ABarrier : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABarrier();
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABarrierElement> BarrierElementClass;

	UPROPERTY()
	TArray<ABarrierElement*> BarrierElements;

	UPROPERTY(EditDefaultsOnly)
	float BlockSize;

	UPROPERTY(EditDefaultsOnly)
	int SizeX;

	UPROPERTY(EditDefaultsOnly)
	int SizeY;

	UPROPERTY(EditDefaultsOnly)
	float StartPosX;
	UPROPERTY(EditDefaultsOnly)
	float StartPosY;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddBarrierElements();
	
};
