// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Barrier.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	BonusTime = 5.f;
	firstTime = true;
	moveDone = true;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);//Update tick every MovementSpeed (per second)
	AddSnakeElements(2);
	FVector NewLocationBarrier(0, 0, 0);
	FTransform NewTransformBarrier(NewLocationBarrier);
	ABarrier* NewBarrier = GetWorld()->SpawnActor<ABarrier>(BarrierClass, NewTransformBarrier);
	Barrier = NewBarrier;
	Barrier->AddBarrierElements();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElements(int ElementsNum)//without default value, only in .h 
{
	for (int i = 0; i < ElementsNum; ++i) {
		FVector NewLocation;
		if(firstTime){
			FVector Temp(SnakeElements.Num() * ElementSize, 0, 0);
			NewLocation = Temp;
		} else {
			NewLocation = GetPosNewElement();
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner=this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
	firstTime = false;
}


//Get location for new element after got bonus to add
FVector ASnakeBase::GetPosNewElement()
{
	auto LastElement = SnakeElements[SnakeElements.Num() - 1];
	auto PrevLastElement = SnakeElements[SnakeElements.Num() - 2];

	FVector LastElementLocation = LastElement->GetActorLocation();
	FVector PrevLastElementLocation = PrevLastElement->GetActorLocation();
	FVector NewLocation (0, 0, LastElementLocation.Z);
		
	if (LastElementLocation.X == PrevLastElementLocation.X) NewLocation.X = LastElementLocation.X;
	else if(LastElementLocation.X < PrevLastElementLocation.X)NewLocation.X = LastElementLocation.X- ElementSize;
	else NewLocation.X = LastElementLocation.X + ElementSize;

	if (LastElementLocation.Y == PrevLastElementLocation.Y) NewLocation.Y = LastElementLocation.Y;
	else if (LastElementLocation.Y < PrevLastElementLocation.Y)NewLocation.Y = LastElementLocation.Y - ElementSize;
	else NewLocation.Y = LastElementLocation.Y + ElementSize;

	return NewLocation;
}



void ASnakeBase::Move()
{
	float MovementBySize = ElementSize;
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection) {
	case EMovementDirection::UP:
			MovementVector.X += MovementBySize;
			break;
	case EMovementDirection::DOWN:
			MovementVector.X -= MovementBySize;
			break;
	case EMovementDirection::LEFT:
			MovementVector.Y += MovementBySize;
			break;
	case EMovementDirection::RIGHT:
			MovementVector.Y -= MovementBySize;
			break;
	}

	//AddActorWorldOffset(MovementVector);
	
	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	moveDone = true;
}


void ASnakeBase::BoostSpeed() {
	MovementSpeed /= 2;
	SetActorTickInterval(MovementSpeed);
	GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Blue, TEXT("Speed boost activated"));
	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, this, &ASnakeBase::RemoveSpeed, BonusTime, false);
}

void ASnakeBase::RemoveSpeed() {
	MovementSpeed *= 2;
	SetActorTickInterval(MovementSpeed);
	GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Blue, TEXT("Speed boost is over"));
}

int ASnakeBase::getSizeXBarrier()
{
	return Barrier->SizeX;
}

float ASnakeBase::getBlockSizeBarrier()
{
	return Barrier->BlockSize;
}

float ASnakeBase::getStartPosXBarrier()
{
	return Barrier->StartPosX;
}
float ASnakeBase::getStartPosYBarrier()
{
	return Barrier->StartPosY;
}
int ASnakeBase::getSizeYBarrier()
{
	return Barrier->SizeY;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

