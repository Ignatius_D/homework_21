// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoost.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBoost::ASpeedBoost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASpeedBoost::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBoost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBoost::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->BoostSpeed();
			//this->Destroy();
			float BlockSize = Snake->getBlockSizeBarrier();
			float StartPosX = Snake->getStartPosXBarrier();
			float StartPosY = Snake->getStartPosYBarrier();
			int SizeX = Snake->getSizeXBarrier();
			int SizeY = Snake->getSizeYBarrier();

			std::srand(std::time(nullptr));
			float x = StartPosX + (std::rand() % (SizeX - 2) + 1) * BlockSize;
			float y = StartPosY + (std::rand() % (SizeY - 2) + 1) * BlockSize;
			FVector NewLocation(x, y, 0);
			this->SetActorLocation(NewLocation);
		}
	}
}



